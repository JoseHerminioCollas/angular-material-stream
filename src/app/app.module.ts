import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { AppComponent } from './app.component';
import { ToolBar } from './toolbar/toolbar';
import {DialogsModule } from './dialog/dialogs.module';

@NgModule({
  declarations: [
    AppComponent,
    ToolBar
  ],
  imports: [
    MaterialModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    DialogsModule
    ],
  providers: [],
    bootstrap: [
    AppComponent
    ]
})
export class AppModule {}
