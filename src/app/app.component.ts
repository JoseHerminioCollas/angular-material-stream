import {Component, OnInit, EventEmitter} from '@angular/core';
import {ViewContainerRef} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/fromEvent';
import {AppEvent} from './app-event-interface'
import {DialogsService} from './dialog/dialogs.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title: string;
  clickCount = 20; // initialize with this number
  applicationControl: EventEmitter<AppEvent> = new EventEmitter<AppEvent>(); //applicationStream
  obs: Observable<any>; // openStream
  obsClose: Observable<AppEvent>; // closeStream
  public result: any;

  constructor(
      private dialogsService: DialogsService,
      private viewContainerRef: ViewContainerRef
    ) {
        this.title = 'Streams!';
        this.setStream()
        this.setDocEvent()
        this.initEvent();
  }
  setStream(){
      this.obs = this.applicationControl
      .filter(x => {
        return x.action === 'open' && x.content === 'confirmQuery';
      });
      this.obs.subscribe(x => {
        this.dialogsService
          .confirm(' - - - ', 'Are you sure you want to do this?', this.viewContainerRef)
          .subscribe(res => {
              this.result = res
            });
      });

      this.obsClose = this.applicationControl
      .filter(x => {
        return x.action === 'open' && x.content === 'about';
      });
      this.obsClose.subscribe(x => {
        this.dialogsService
          .information('Streams and Angular', 'Explorations in methodologies.', this.viewContainerRef)
        console.log('close obs !!!!', x)
      });
  }
  setDocEvent(){
    Observable.fromEvent(document, 'click')
    .subscribe(x => {
      console.log(123, x, event)
    });
  }
  initEvent(){
    this.applicationControl.emit(
        {name: 'Open Dialog About', action: 'open', target: 'dialog', content: 'about'}
      );
  }
  callObs(){
    let e = Observable.from([1, 2, 3]);
    e.subscribe(x => {
      console.log(x, window, document)
    })
  }
  // subscribe this to the change events of components
  //         (change)="countChange($event)">
  countChange(e: number) {
     this.clickCount = e;
   }
}

