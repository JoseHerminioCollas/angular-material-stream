import {Component, Injectable, ViewContainerRef } from '@angular/core';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { Observable } from 'rxjs/Rx';
import { ConfirmDialog } from './confirm-dialog.component';

@Injectable()
export class DialogsService {
    dialogRef: MdDialogRef<ConfirmDialog>; // <DialogContent>
    aboutDR: MdDialogRef<DialogContent>
    config: MdDialogConfig;
    constructor(private dialog: MdDialog) {
        this.config = new MdDialogConfig();
    }

    public information(title: string, message: string, viewContainerRef: ViewContainerRef): 
    Observable<boolean> {
        console.log('info...', title)
        this.config.viewContainerRef = viewContainerRef;
        this.aboutDR = this.dialog.open(DialogContent, this.config);
        this.aboutDR.componentInstance.title = title;
        this.aboutDR.componentInstance.message = message;

        return this.aboutDR.afterClosed();
    }

    public confirm(title: string, message: string, viewContainerRef: ViewContainerRef): 
    Observable<boolean> {
        this.config.viewContainerRef = viewContainerRef;
        this.dialogRef = this.dialog.open(ConfirmDialog, this.config);
        this.dialogRef.componentInstance.title = title;
        this.dialogRef.componentInstance.message = message;

        return this.dialogRef.afterClosed();
    }
}

@Component({
    selector: 'confirm-dialog',
    template: `
        <h3>{{title}}</h3>
        <p>{{message}}</p>
    `
})
export class DialogContent {
    public title: string;
    public message: string;

    constructor(public dialogRef: MdDialogRef<ConfirmDialog>) {}
}