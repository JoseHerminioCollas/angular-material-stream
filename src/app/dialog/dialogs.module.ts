import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { DialogsService, DialogContent } from './dialogs.service';
import { ConfirmDialog } from './confirm-dialog.component';

@NgModule({
    imports: [
        MaterialModule.forRoot(),
    ],
    exports: [
        ConfirmDialog, DialogContent
    ],
    declarations: [
        ConfirmDialog, DialogContent
    ],
    providers: [
        DialogsService,
    ],
    entryComponents: [
        ConfirmDialog, DialogContent
    ],
})
export class DialogsModule { }
