export interface AppEvent {
    name: string;
    action?: string;
    target?: string;
    content?: string;
}
/*

{name: 'Close About Dialog', action: 'open', target: 'about'}
{name: 'Open About Dialog', action: 'close', target: 'about'}

{name: 'User Response', action: 'query', response: 'yes'}
{name: 'User Response', action: 'query', response: 'No'}
{name: 'User Response', action: 'query', response: COLOR_CHOICES['red']}

*/