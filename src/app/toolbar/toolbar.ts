import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AppEvent} from '../app-event-interface'; //TODO relative paths

@Component({
  selector: 'toolbar',
  styleUrls: ['./toolbar.css'],
  templateUrl: './toolbar.html'
})
export class ToolBar{
  @Input()
  count = 0; // initilize at template or not
  @Input()
  applicationStream: EventEmitter<AppEvent>; // applicationStream

  @Output()
  change: EventEmitter<number> = new EventEmitter<number>(); // incrementCount

  openMenu(e)  {
    this.applicationStream.emit(
      {name: 'Open Dialog', action: 'open', target: 'dialog', content: 'about'}
    )
  }
  openDialog(e) {
    console.log('e', e, event)
    this.applicationStream.emit(
      {name: 'Open Dialog', action: 'open', target: 'dialog', content: 'confirmQuery'}
    )
  }
  incrementCount(e) {
    this.count++;
    this.change.emit(this.count);
  }
}

